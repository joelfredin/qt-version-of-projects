# QT Console Applications
This project is made up of several of my old projects, where I have merged C++ project, to the qt-framework.

# Requirements
This project depends on

- [QT](https://www.qt.io/)

# Install
You need to download the qt-framework and then you can open the project in that application. Then you press the green arrow button which says "Run", to run the project.

# Usage

# Contributing
Joel Fredin