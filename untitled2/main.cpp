#include <QCoreApplication>

#include<iostream>
#include<string>
#include<vector>
#include<typeinfo>

// Vectors used to convert strings describing numbers, to int data types.
std::vector<QString> ones = {"zero", "one","two","three","four","five","six","seven","eight", "nine"};
std::vector<QString> tens = {"ten","twenty","thirty","forty","fifty","sixty","seventy","eighty", "ninety"};
std::vector<QString> hundred = {"one hundred","two hundred","three hundred","four hundred","five hundred","six hundred","seven hundred","eight hundred", "nine hundred"};


// Represents all elements of ones, as ints.
std::vector<int> convert_vector_string_to_number()
{
    std::vector<int> numerals(10);

    for(int i = 0; i < ones.size()+1; i++)
    {
        numerals[i] = i;
    }
    return numerals;
}

// Converts an integer from 0 to 9 represented as strings, to an int.
int convert_string_to_number(QString n)
{
    for(int i = 0; i < ones.size(); i++)
    {
        if(n == ones[i])
        {
            return convert_vector_string_to_number()[i];
        }
    }
    return 0;
}

// Represents all elements of tens, as ints.
std::vector<int> convert_vector_string_to_number2()
{
    std::vector<int> numerals(10);

    for(int i = 0; i < tens.size()+1; i++)
    {
        numerals[i] = 10*(i+1);
    }

    return numerals;
}


// Converts integers 10^x represented as strings, where x ranges from 1 to 9, to an int.
int convert_string_to_number2(QString n)
{
    for(int i = 0; i < tens.size(); i++)
    {
        if(n == tens[i])
        {
            return convert_vector_string_to_number2()[i];
        }
    }
    return 0;
}

// Represents all elements of hundred, as ints.
std::vector<int> convert_vector_string_to_number3()
{
    std::vector<int> numerals(10);

    for(int i = 0; i < tens.size()+1; i++)
    {
        numerals[i] = 100*(i+1);
    }

    return numerals;
}

// Converts 100^x represented as strings, to ints.
int convert_string_to_number3(QString n)
{
    for(int i = 0; i < hundred.size(); i++)
    {
        if(n == hundred[i])
        {
            return convert_vector_string_to_number3()[i];
        }
    }
    return 0;
}

// Given a string which contains an operator, find its position.
int find_index_of_symbol(QString my_string)
{
    if(my_string.contains("plus") != -1)
    {
        return my_string.contains("plus");
    }
    if(my_string.contains("minus") != -1)
    {
        return my_string.contains("minus");
    }
    if(my_string.contains("times") != -1)
    {
        return my_string.contains("times");
    }
    if(my_string.contains("divided by") != -1)
    {
        return my_string.contains("divided by");
    }
    return -1;
}

// Given a string which contains an operator, find the position of the next word.
int find_second_index_of_symbol(QString my_string)
{
    if(my_string.contains("plus") != -1)
    {
        return my_string.contains("plus")+4;
    }
    if(my_string.contains("minus") != -1)
    {
        return my_string.contains("minus")+5;
    }
    if(my_string.contains("times") != -1)
    {
        return my_string.contains("times")+5;
    }
    if(my_string.contains("divided by") != -1)
    {
        return my_string.contains("divided by")+10;
    }
    return -1;
}

QString generate_word(QString word, int index)
{
    QString my_new_string;
    for(int i = index; i < word.length(); i++)
    {
        if(word[i] == ' ')
        {
            return my_new_string;
        }
        else
        {
            my_new_string += word[i];
        }
    }
    return "";
}

// Finds the substring to the left of the operator.
QString find_first_number(QString my_string)
{
    int my_number = find_index_of_symbol(my_string);
    return my_string.mid(0, my_number-1);
}

// Finds the substring to the right of the operator.
QString find_second_number(QString my_string)
{
    int my_number = find_second_index_of_symbol(my_string);
    return my_string.mid(my_number+1,my_string.length());
}

// Given a string which represents a number, find the position of the number which represents hundred.
int find_hundred(QString my_string)
{
    if(my_string.contains("hundred") != -1)
    {
        return my_string.contains("hundred")+8;
    }
    return -1;
}

// Given a string which represents a number, find the position of the number which represents the tenth.
int find_tenth(QString my_string)
{
    if(my_string.contains("ty") != -1)
    {
        return my_string.contains("ty")+3;
    }
    return -1;
}

// returns the substring which represents the hundreth in a base-10 expansion.
QString compute_hundred(QString my_string)
{
    QString a = my_string;
    if(my_string.contains("hundred") == -1)
    {
        return "zero";
    }
    int my_number = find_hundred(my_string);
    return a.mid(0, my_number-1);
}

// returns the substring which represents the tenth in a base-10 expansion.
QString compute_tenth(QString my_string)
{
    QString a = my_string;
    if(my_string.contains("ty") == -1)
    {
        return "zero";
    }
    int my_number2 = find_tenth(a);
    int my_number = find_hundred(a);
    QString wordis = generate_word(a,my_number);
    return wordis;
}

// returns the substring which represents the ones in a base-10 expansion.
QString compute_ones(QString my_string)
{
    QString a = my_string;
    if(a.contains("ty") == -1)
    {
        if(a.contains("hundred") != -1)
        {
            return a.mid(find_hundred(my_string), a.length());
        }
        return my_string;
    }
    int my_number = find_tenth(my_string);
    return a.mid(my_number, a.length());
}


int operatoris(QString mathematical_statement)
{
    QString left_number =  find_first_number(mathematical_statement);
    QString right_number = find_second_number(mathematical_statement);

    QString my_hundred = compute_hundred(left_number);
    QString my_tenth = compute_tenth(left_number);
    QString my_ones = compute_ones(left_number);

    QString my_hundred2 = compute_hundred(right_number);
    QString my_tenth2 = compute_tenth(right_number);
    QString my_ones2 = compute_ones(right_number);

    int representation1 = convert_string_to_number(my_ones);
    int representation2 = convert_string_to_number2(my_tenth);
    int representation3 = convert_string_to_number3(my_hundred);

    int representation4 = convert_string_to_number(my_ones2);
    int representation5 = convert_string_to_number2(my_tenth2);
    int representation6 = convert_string_to_number3(my_hundred2);


    if(mathematical_statement.contains("plus") != -1)
    {
        return (representation1 + representation2 + representation3 + representation4 + representation5 + representation6);
    }
    if(mathematical_statement.contains("minus") != -1)
    {
        return (representation1 + representation2 + representation3) - (representation4 + representation5 + representation6);
    }
    if(mathematical_statement.contains("times") != -1)
    {
        return (representation1 + representation2 + representation3) * (representation4 + representation5 + representation6);
    }
    if(mathematical_statement.contains("divided by") != -1)
    {
        return (float)(representation1 + representation2 + representation3) / (representation4 + representation5 + representation6);
    }

    return 0;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);



    std::cout << "Hello and welcome to the world of mathematical statements. Please enter a statement you want us to interpret and operate on." << std::endl;
    std::cout << "The operations you are allowed to do are plus, minus, times and divided by." << std::endl;
    std::cout << "The statements must be on the following form: X operator Y, here comes a few examples:" << std::endl << std::endl;
    std::cout << "Example 1: two plus three" << std::endl;
    std::cout << "Example 2: three times five" << std::endl;
    std::cout << "Example 3: twenty two minus six" << std::endl;
    std::cout << "Example 4: five divided by four" << std::endl << std::endl;
    std::cout << std::endl << std::endl;
    std::cout << "Please, go a head and write your own statement:" << std::endl;
    QString math_statement;
    QTextStream qin(stdin);

       qin >> math_statement;

    std::cout << operatoris(math_statement) << std::endl;
    return a.exec();
}
